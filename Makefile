#
# Wikipelago Makefile.
#

# Folder relative to this Makefile pointing to where the Onion MkDocs is
# installed Customize to your needs in your main Makefile. Examples:
ONION_MKDOCS_PATH = vendors/onion-mkdocs

# This is useful when developing locally and Onion MkDocs is not yet installed
vendoring:
	@test   -e $(ONION_MKDOCS_PATH) && git -C $(ONION_MKDOCS_PATH) pull || true
	@test ! -e $(ONION_MKDOCS_PATH) && git clone https://gitlab.torproject.org/tpo/web/onion-mkdocs.git $(ONION_MKDOCS_PATH) || true

# Include Onion MkDocs Makefiles.
# See https://www.gnu.org/software/make/manual/html_node/Include.html
#
# Some of those files might even contain local customizations/overrides
# that can be .gitignore'd, like a Makefile.local for example.
-include $(ONION_MKDOCS_PATH)/Makefile.onion-mkdocs

# Check for broken links
broken-links:
	@pipenv run mkdocs-linkcheck -r --local docs | tee logs/brokenlinks.log
