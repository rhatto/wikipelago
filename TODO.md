# TODO - Wikipelago

Tasks not yet moved to the [issue queue][], as sometimes editing a TODO file is
way quicker.

## Bootstrapping

* [x] Time tracking: around ~1 without rush to have a working setup.
* [x] Legacy Wiki.
* [x] Setup scheduled pipelines:
      https://gitlab.torproject.org/rhatto/wikipelago/-/pipeline_schedules

## Organization

* [~] Move these tasks into tickets at the [issue queue][].
      Upstreamed into https://gitlab.torproject.org/rhatto/wikipelago/-/issues/1
* [ ] Licensing:
  * [x] Setup LICENSE.
  * [ ] Determine whether the wikis falls under the same license. Is it the
        same as the [official copyright
        policy](https://www.torproject.org/about/trademark/)?

## Fixes

* [ ] Switch compilation scheme to use two folders, [like in the Onion Services
      Ecosystem docs][]:
      * [ ] A `repos/` folder to store the repositories, which are symlinked on
            `docs/`.
      * [ ] Update `.gitignore` to stop ignoring the `docs/` folder.
* [ ] Fix internal links.
* [ ] Handle redirects: MkDocs plugin + .htaccess + GitLab wiki redirects.
* [ ] PDF handling:
  * [x] Do not host PDFs.
  * [ ] Try to convert PDF references to links pointing to the corresponding
        GitLab wiki.

[like in the Onion Services Ecosystem docs]: https://gitlab.torproject.org/tpo/onion-services/ecosystem/-/blob/main/scripts/compile

## Features

* [~] Try the [multi-repository
      approach](https://pypi.org/project/mkdocs-multirepo-plugin/).
      Won't work right now, since the wikis needs pre-processing
      before compilation. They can't be used directly from upstream,
      as-is.
* [ ] Spell checking.

## Diagnostics

* [ ] Diagnostics via CI:
  * [ ] Broken links (`make broken-links`).
  * [ ] Number of pages.
  * [ ] Wiki sizes.
  * [ ] Markdown syntax.

### Reporting

* [x] Diagnostics (broken links, PDF situation, etc).
* [ ] Slides.

[issue queue]: https://gitlab.torproject.org/rhatto/wikipelago/-/issues
