# Wikipelago Experiment Report

* Initial version: 2023-11.
* Last recorded update: 2024-06.

## Introduction

This is the report detailing the potential and the challenges involved in using
a static site generator solution to converge many [GitLab Wiki][] instances
from the [Tor Project][] into a single place.

[GitLab Wiki]: https://docs.gitlab.com/ee/user/project/wiki/
[Tor Project]: https://torproject.org

## Setup

### Chosen solution

[Onion MkDocs][] was chosen in this experiment, for a few reasons:

* It's a static site compiler which supports almost the same syntax from
  [GitLab Wiki][].
* It comes with a default setup easy to vendorize in any project, to integrate
  with GitLab CI and has a working theme for Tor-related projects.
* It was already being used in some Tor projects.
* This experiment was part of a [Onion MkDocs tryout][] during the 2023 [Tor Hackweek][]

It's worth note that this experiment could have used another solution: what matters
is how the multiple [GitLab Wiki][] instances were assembled in the same place, rather
than which specific static generator was employed.

[Onion MkDocs]: https://tpo.pages.torproject.net/web/onion-mkdocs/
[Onion MkDocs tryout]: https://gitlab.torproject.org/tpo/community/hackweek/-/issues/13
[Tor Hackweek]: https://tpo.pages.torproject.net/community/hackweek/

### How it works

The Tor Wikipelago Experiment works by combining multiple [GitLab Wikis from The
Tor Project][] into a single statically generated website through the following
workflow:

```mermaid
flowchart TD
    A[CI Pipeline] --> B(Compile)
    B --> C{Clone Wiki Repositories}
    C --> D[TPO]
    C --> E[Community Team]
    C --> F[etc]
    D --> G[Apply Fixes]
    E --> G
    F --> G
    G --> H[MkDocs Build]
    H --> I[GitLab Pages]
```

This procedure runs as a [scheduled job][], ensuring the wikipelago has recent copies
of all tracked [GitLab Wiki][] instances.

[GitLab Wikis from The Tor Project]: https://gitlab.torproject.org/tpo/team/-/wikis/home
[scheduled job]: https://gitlab.torproject.org/rhatto/wikipelago/-/pipeline_schedules

## Diagnostics

This section gathers some diagnostics of the existing wikis.

### Wiki sizes

Repository sizes[^repository-sizes]:

| Wiki            | Repository size
|-----------------|----------------
| legacy          | 273M
| tpo             | 188M
| ux              | 54M
| tpa             | 25M
| community       | 9,2M
| operations      | 7,7M
| network-health  | 7,4M
| web             | 3,3M
| anti-censorship | 2,2M
| core            | 1,4M
| applications    | 996K
| onion-services  | 936K
| Total           | 571M

[^repository-sizes]: Data gathered with `du -h -d 1 docs`

### Number of pages

Number of pages, total and per repository[^number-pages-repo]:

| Wiki            | Number of pages
|-----------------|----------------
| legacy          | 1609
| tpo             | 1168
| tpa             | 215
| network-health  | 56
| core            | 37
| anti-censorship | 36
| operations      | 32
| community       | 30
| applications    | 29
| web             | 26
| onion-services  | 24
| ux              | 5
| Total           | 3269

[^number-pages-repo]: Data gathered with
                      `find docs -name '*.md' | wc -l` and
                      `for file in docs/*; do find $file -name '*.md' | wc -l | xargs echo "| $(basename $file) | "; done`

### Broken links

Broken links were detected _after_ compiling and building the Wikipelago
using [MkDocs][], which means these may be either already existing in the
[GitLab Wiki][] instances or not being detected by [MkDocs][] due to an
unsupported link syntax.

Broken links were discovered by running `mkdocs build` directly, or also
through the [mkdocs-linkcheck][] plugin:

```
make broken-links
```

Summary report:

```
Total files checked: 3269
Total links checked: 2940
        Local links: 2940
       Remote links: 0
        Empty links: 4
       Broken links: 2935
      Skipped links: 0
```

[MkDocs]: https://www.mkdocs.org
[mkdocs-linkcheck]: https://pypi.org/project/mkdocs-linkcheck/

### Wiki structures

* Many pages are not well structured, and have broken syntax.

### Compression and artifact size

* If assets are not compresed, artifact size is bigger than 3GB, with
  compression it gets around 800MB (not counting the Legacy Wiki).
* PDFs were removed to keep the artifacts size small.
* Artifacts gets too large for GitLab Pages the Legacy Wiki is included.

### Search

Considering the large number of documents, the search is performing well better
than not having one.

The size of the search index was determined, as it has been [reported that it's
a too big for limited connections][]:

* Default `search_index.json`: 9,6M (this is what is served by MkDocs)
* After gzipping: 3,0M
* After xzipping: 2,1M

A compressed index could improve the situation.

### Performance

It could be possible to get some more numbers, like:

* Profiling in the browser how long it takes to do page loading and search.

[reported that it's a too big for limited connections]: https://gitlab.torproject.org/tpo/community/hackweek/-/issues/13#note_2962989

### Conclusion

* The Wikipelago Experiment attempted to determine what is possiblie to do with
  minimal changes in the existing wikis.

* With changes, much more can be done.

## Discussion

### Fixing broken links

At first, it was considered to try a quick regexp converting all links with
slashes not begining with http into absolute links.

But the like the conversion logic should account for a slightly more
complicated situation, as [discussed in TPA's documentation about
documentation systems][]:

* Markdown links may use either `[Title](addr)`, `[Title]: addr` or `<a
  href="addr">Title</a>` syntaxes.
* The link block may be multiline.
* [Link types in GitLab Wikis][] may be "direct page", "direct file",
  "hierarchical" and "root".

A possible solutions could involve:

* For each wiki:
    * For each link syntax:
      * Parse all the links in a file, with a multiline regexp. For each link:
          * Detect the link type ("direct page", "direct file" etc).
          * Decide to which file this link should point to, taking into account the
            current file path.
          * Check whether the destination file exists.
          * Substite the link directly in the file.

Another approach:

* For each wiki:
    * Use a library like [mistune][] to parse the Markdown files.
    * Iterate in the [AST][], and for each link:
        * Detect the link type ("direct page", "direct file" etc).
        * Decide to which file this link should point to, taking into account the
          current file path.
        * Check whether the destination file exists.
        * Replace the link in the [AST][].
    * Convert the [AST][] into an HTML file.

[Link types in GitLab Wikis]: https://docs.gitlab.com/ee/user/markdown.html#wiki-specific-markdown
[discussed in TPA's documentation about documentation systems]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/documentation#mkdocs
[mistune]: https://mistune.lepture.com/
[AST]: https://mistune.lepture.com/en/latest/guide.html#abstract-syntax-tree

### Link preservation in future migrations

* Fixing the docs and preserving old URLs may be two incompatible goals.
* Cool URLs does not change, but the state of the docs may be well beyond cool.
* Preserving links at this point can be harmful in the sense that it's going
  very hard to implement redirections in the [GitLab Wiki][], since it relies in
  an engine without this feature:
    * [Gitlab wiki redirections and other gollum features - How to Use GitLab - GitLab Forum](https://forum.gitlab.com/t/gitlab-wiki-redirections-and-other-gollum-features/42993)
    * [support redirection system (#5) · Issues · GitLab.org / gollum-lib · GitLab](https://gitlab.com/gitlab-org/gollum-lib/-/issues/5)
* In the other hand, in a future migration away from [GitLab Wiki][], it could
  be possible to add a link to every new page by mass editing the wikis using a
  script.

### Multiple repositories

Another way to build Wikipelago would be combining each wiki as if it was it's
own MkDocs site. There are two ways do to this:

* [mkdocs-multirepo-plugin · PyPI](https://pypi.org/project/mkdocs-multirepo-plugin/)
* [Built-in projects plugin - Material for MkDocs](https://squidfunk.github.io/mkdocs-material/plugins/projects/)

## Recommendations

* Most wikis are small and can be easily fixed.

* Most wikis need a better folder/topic/content structure.

* Many pages needs proper document structure (sections, subsections etc) and
  Markdown syntax fixes.

* Some wikis have many PDFs, which increase the GitLab Pages artifact size.
  One solution would be to not include the PDF in the static page, and link
  the PDFs using the GitLab repository URLs instead.

* [TPO wiki][] is the biggest, and needs major re-structuring, re-organizing
  and fixes.

* Adopt Markdown as the default syntax for documentation at Tor, not just
  for wikis but also for [collaborative editing][].

* It may also be interesting to discuss whether to adopt a specific Mardown
  flavor (like [GitLab Flavored Markdown][], be strict on the standard Markdown
  or to be undefined on this.

* Remark about taking care with the number of third party plugins.

* Proposal for converting the GitLab wikis into regular repositories
  and use a https://docs.torproject.org as an integration site.

* Consider to update the [Tor User Documentation Style Guide][] with the
  adopted recommendations regarding formatting, structuring etc.

[TPO wiki]: https://gitlab.torproject.org/tpo/team/-/wikis/home
[collaborative editing]: https://gitlab.torproject.org/tpo/community/hackweek/-/issues/16
[GitLab Flavored Markdown]: https://docs.gitlab.com/ee/user/markdown.html
[Tor User Documentation Style Guide]: https://gitlab.torproject.org/tpo/team/-/wikis/Tor-User-Documentation-Style-Guide

## References

### Editing

During discussions, the following plugin was mentioned (although it seems not to
have the ability to create new pages):

* [mkdocs-live-edit-plugin · PyPI](https://pypi.org/project/mkdocs-live-edit-plugin/)

### Searching

Some references to understand how the search functionality can be improved:

Improvements for the current setup:

* MkDocs Material search functionality:
    * Existing implementation (as of 2024-06):
        * [Built-in search plugin - Material for MkDocs](https://squidfunk.github.io/mkdocs-material/plugins/search/?h=lunr)
        * [Search: better, faster, smaller - Material for MkDocs](https://squidfunk.github.io/mkdocs-material/blog/2021/09/13/search-better-faster-smaller/#search-index-docspagemd)
        * [Setting up site search - Material for MkDocs](https://squidfunk.github.io/mkdocs-material/setup/setting-up-site-search/?h=)
            * [Setting up site search - Fork of Material for MkDocs](https://jimandreas.github.io/mkdocs-material/setup/setting-up-site-search/)
    * Upcoming implementation (as of 2024-06):
        * [Towards better documentation search · Issue #6307 · squidfunk/mkdocs-material · GitHub](https://github.com/squidfunk/mkdocs-material/issues/6307)
* [Lunr][]:
    * [Large search indexes can cause Lunr to freeze the page while it creates the index. · Issue #859 · mkdocs/mkdocs](https://github.com/mkdocs/mkdocs/issues/859)
    * [Upgrade to lunr 2.x · Issue #1319 · mkdocs/mkdocs](https://github.com/mkdocs/mkdocs/issues/1319)
    * [olivernn/lunr.js: A bit like Solr, but much smaller and not as bright](https://github.com/olivernn/lunr.js)
        * [Lunr with a large index (800,000 items) · Issue #222 · olivernn/lunr.js](https://github.com/olivernn/lunr.js/issues/222)
* [Elasticlunr.js][]: could be an alternative to [Lunr][].

[Lunr]: https://lunrjs.com
[Elasticlunr.js]: http://elasticlunr.com

Offline search support:

* [Offline search](https://jimandreas.github.io/mkdocs-material/setup/setting-up-site-search/#offline-search)
* [Building for offline usage - Material for MkDocs](https://squidfunk.github.io/mkdocs-material/setup/building-for-offline-usage/)

Customized search (using an external backend):

* [Custom search](https://jimandreas.github.io/mkdocs-material/setup/setting-up-site-search/#custom-search):
    * This could solve the search index size problem.
    * But needs a middleware between the search backend and MkDocs.
    * One way to achieve this would be running Lunr server side, so the
      existing frontend code could be adapted/moved to a backend that just does
      searching. A search service could event be capable of operating with
      multiple indexes, so a single service could handle searching for multiple
      MkDocs sites.

## Notes
