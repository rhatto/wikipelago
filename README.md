# Tor Project Wikipelago Experiment

## About

This is part of the [2023 Hackweek's Onion MkDocs Tryout][].

## Browsing

* Check the [docs](docs) folder for the basic project details.
* Browse the [live site][] to access the full documentation!

## Tasks

Check the [TODO](TODO.md) file for the project tasks.

[2023 Hackweek's Onion MkDocs Tryout]: https://gitlab.torproject.org/tpo/community/hackweek/-/issues/13
[live site]: https://rhatto.pages.torproject.net/wikipelago/
