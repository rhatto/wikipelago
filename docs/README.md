# Tor Project Wikipelago Experiment

This project is an attempt at converging the Tor Project's wiki archipelago
into the same rendering and search engines:

* This is part of the [2023 Hackweek's Onion MkDocs Tryout][].
* Use the menu and the search box to find content.
* Read the [full report](report.md) with the experiment results.

[2023 Hackweek's Onion MkDocs Tryout]: https://gitlab.torproject.org/tpo/community/hackweek/-/issues/13
